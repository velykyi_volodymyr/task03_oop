/**
 * This is a part of task03 for Epam online Java course.
 */
package com.velykyi.controller;

import com.velykyi.model.*;

import java.util.*;

/**
 * This class will be using for control our model.
 *
 * @author Vilidymyr Velykyi
 */
public class Controller implements ControllerInterface {
    /**
     * This is our model.
     */
    private Model model;

    /**
     * Initializes our model.
     *
     * @param model is our model.
     */
    public Controller(Model model) {
        this.model = model;
    }

    /**
     * Create a list of price by method from our model.
     *
     * @return list of price.
     */
    public List<Double> createPriceList() {
        return model.createPriceList();
    }

    /**
     * Add a new item to order by method from our model.
     *
     * @param item   is new flower or flowerpot.
     * @param amount is amount of flower or size of flowerpot.
     */
    public void addNewItem(Plant item, double amount) {
        model.addNewItem(item, amount);
    }

    /**
     * Print check be method from our model.
     */
    public void printCheck() {
        model.printCheck();
    }
}
