/**
 * This is a part of task03 for Epam online Java course.
 */
package com.velykyi.controller;

import com.velykyi.model.Plant;

import java.util.*;

/**
 * This interface describes controller.
 *
 * @author Vilidymyr Velykyi
 */
public interface ControllerInterface {
    /**
     * Create a list of price by method from our model.
     *
     * @return list of price.
     */
    List<Double> createPriceList();

    /**
     * Add a new item to order by method from our model.
     *
     * @param item   is new flower or flowerpot.
     * @param amount is amount of flower or size of flowerpot.
     */
    void addNewItem(Plant item, double amount);
    /**
     * Print check be method from our model.
     */
    void printCheck();
}
