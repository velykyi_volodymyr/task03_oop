/**
 * This is a part of task03 for Epam online Java course.
 */
package com.velykyi.model;

/**
 * This class is model of flower.
 *
 * @author Volodymyr Velykyi
 */
public class Flower extends Plant {
    /**
     * Color of flower.
     */
    private String color;

    /**
     * Initialize flower.
     *
     * @param name  is name of flower.
     * @param price is price of one flower.
     * @param color color of flower.
     */
    public Flower(String name, int price, String color) {
        super(name, price);
        this.color = color;
    }

    /**
     * Initialize flower.
     *
     * @param name  is name of flower.
     * @param price is price of one flower.
     */
    public Flower(String name, int price) {
        super(name, price);
    }


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    /**
     * Create format of flower for print.
     *
     * @return string of flower.
     */
    @Override
    public String toString() {
        if (color == null) {
            return getName() + " " + getPrice() + " for 1 item";
        } else {
            return getName() + " " + getColor() + " " + getPrice()
                    + " for 1 item";
        }
    }
}
