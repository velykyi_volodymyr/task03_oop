/**
 * This is a part of task03 for Epam online Java course.
 */
package com.velykyi.model;

/**
 * This class is model of flower.
 *
 * @author Volodymyr Velykyi
 */
public class Flowerpot extends Plant {
    /**
     * Size of flowerpot.
     */
    private String size;

    /**
     * Initialize flowerpot.
     *
     * @param name is name of flowerpot.
     * @param price is price of flowerpot.
     * @param size is size of flowerpot.
     */
    public Flowerpot(String name, int price, String size) {
        super(name, price);
        this.size = size;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }


    /**
     * Create format of flowerpot for print.
     *
     * @return string of flowerpot.
     */
    @Override
    public String toString() {
        return getName() + " " + getSize() + " " + getPrice() + " for 1 item";
    }
}
