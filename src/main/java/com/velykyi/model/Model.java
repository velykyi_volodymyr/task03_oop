/**
 * This is a part of task03 for Epam online Java course.
 */
package com.velykyi.model;

import java.util.*;

/**
 * This interface describes model.
 *
 * @author Vilidymyr Velykyi
 */
public interface Model {
    /**
     * Create sorted list of price of our order.
     *
     * @return list of price.
     */
    List<Double> createPriceList();

    /**
     * Add new item to our order.
     *
     * @param item   is item which will be added.
     * @param amount is amount of flower or size of flowerpot.
     */
    void addNewItem(Plant item, double amount);

    /**
     * Print check of our order.
     */
    void printCheck();
}
