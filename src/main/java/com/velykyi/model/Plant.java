/**
 * This is a part of task03 for Epam online Java course.
 */
package com.velykyi.model;

/**
 * This class is model of flower.
 *
 * @author Volodymyr Velykyi
 */
public class Plant {
    /**
     * Name of plant.
     */
    private String name;
    /**
     * Price of plant.
     */
    private int price;

    /**
     * Initialize plant.
     *
     * @param name is name of plant.
     * @param price is price of plant.
     */
    public Plant(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }
}
