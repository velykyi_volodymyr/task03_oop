/**
 * This is a part of task03 for Epam online Java course.
 */
package com.velykyi.model;

import java.util.*;

/**
 * This class will be using for create and work with order.
 */
public class BusinessLogic implements Model {
    /**
     * This is our order.
     */
    private static Map<Plant, Double> order;

    /**
     * Initializes our order.
     *
     * @param order is our order.
     */
    public BusinessLogic(Map<Plant, Double> order) {
        this.order = order;
    }

    /**
     * Add new item to our order.
     *
     * @param item   is item which will be added.
     * @param amount is amount of flower or size of flowerpot.
     */
    public void addNewItem(Plant item, double amount) {
        order.put(item, amount);
    }

    /**
     * Create sorted list of price of our order.
     *
     * @return list of price.
     */
    public List<Double> createPriceList() {
        List<Double> priceList = new ArrayList<Double>();
        for (Plant item : order.keySet()) {
            priceList.add(item.getPrice() * order.get(item));
        }
        Collections.sort(priceList);
        return priceList;
    }

    /**
     * Print check of our order.
     */
    public void printCheck() {
        List<Double> sortPrice = createPriceList();
        System.out.println("Check:\n");
        for (Double price : sortPrice) {
            for (Plant item : order.keySet()) {
                if ((order.get(item) * item.getPrice()) == price) {
                    System.out.println(item + " " + price.intValue() + " for "
                            + order.get(item).intValue() + "\n");
                }
            }
        }
    }


}
