/**
 * This is a part of task03 for Epam online Java course.
 */
package com.velykyi;

import com.velykyi.view.View;

/**
 * Start of project.
 */
public class Application {
    public static void main(String[] args) {
        new View().mainMenuShow();
    }
}
