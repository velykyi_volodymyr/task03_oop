/**
 * This is a part of task03 for Epam online Java course.
 */
package com.velykyi.view;

import com.velykyi.controller.Controller;
import com.velykyi.model.*;


import java.util.*;

/**
 * This class is ui which use Controller for work with models.
 */
public class View {
    /**
     * Controller of our model.
     */
    private Controller controller;
    private static Scanner input = new Scanner(System.in);
    /**
     * List of all flowers in shop.
     */
    private static final List<Plant> flowerStorage = new ArrayList<Plant>();
    /**
     * list of all flowerpots in shop.
     */
    private static final List<Plant> flowerpotStorage = new ArrayList<Plant>();
    /**
     * Main menu of ui.
     */
    private static final Map<String, String> mainMenu =
            new LinkedHashMap<String, String>() {
                {
                    put("1", "  1 - make new order");
                    put("2", "  2 - see flower storage");
                    put("3", "  3 - see flowerpot storage");
                    put("Q", "  Q - exit");
                }
            };

    /**
     * Menu for making order.
     */
    private static final Map<String, String> orderMenu =
            new LinkedHashMap<String, String>() {
                {
                    put("1", "  1 - add new item");
                    put("2", "  2 - check");
                    put("B", "  B - back");
                }
            };

    /**
     * Map with methods which execute point of main menu.
     */
    private static final Map<String, Printable> mainMethodMenu =
            new LinkedHashMap<String, Printable>();

    /**
     * Map with methods which execute point of menu for making order.
     */
    private static final Map<String, Printable> orderMethodMenu =
            new LinkedHashMap<String, Printable>();

    /**
     * Initialize lists of flowers and flowerpots and maps of methods.
     */
    public View() {
        flowerStorage.add(new Flower("Rose", 20, "red"));
        flowerStorage.add(new Flower("Rose", 20, "white"));
        flowerStorage.add(new Flower("Chrysanthemum", 15));
        flowerStorage.add(new Flower("Tulip", 25, "pink"));
        flowerStorage.add(new Flower("Tulip", 25, "yellow"));
        flowerStorage.add(new Flower("Tulip", 25, "white"));
        flowerStorage.add(new Flower("Iris", 25));
        flowerStorage.add(new Flower("Peony", 30));
        flowerStorage.add(new Flower("Carnation", 14));
        flowerStorage.add(new Flower("Lily", 30));

        flowerpotStorage.add(new Flowerpot("Сactus", 50, "S"));
        flowerpotStorage.add(new Flowerpot("Сactus", 100, "M"));
        flowerpotStorage.add(new Flowerpot("Сactus", 150, "L"));
        flowerpotStorage.add(new Flowerpot("Azalea", 80, "S"));
        flowerpotStorage.add(new Flowerpot("Azalea", 160, "M"));
        flowerpotStorage.add(new Flowerpot("Azalea", 240, "L"));
        flowerpotStorage.add(new Flowerpot("Hibiscus", 65, "S"));
        flowerpotStorage.add(new Flowerpot("Hibiscus", 130, "M"));
        flowerpotStorage.add(new Flowerpot("Hibiscus", 195, "L"));
        flowerpotStorage.add(new Flowerpot("Gardenia", 110, "S"));
        flowerpotStorage.add(new Flowerpot("Gardenia", 220, "M"));
        flowerpotStorage.add(new Flowerpot("Gardenia", 330, "L"));

        mainMethodMenu.put("1", this::pressMainButton1);
        mainMethodMenu.put("2", this::pressMainButton2);
        mainMethodMenu.put("3", this::pressMainButton3);

        orderMethodMenu.put("1", this::pressOrderButton1);
        orderMethodMenu.put("2", this::pressOrderButton2);

    }

    /**
     * Create new order and open order menu.
     */
    private void pressMainButton1() {
        Map<Plant, Double> order = new LinkedHashMap<Plant, Double>();
        BusinessLogic businessLogic = new BusinessLogic(order);
        controller = new Controller(businessLogic);
        System.out.println("Make your order!");
        orderMenuShow();
    }

    /**
     * Print all flowers.
     */
    private void pressMainButton2() {
        for (Plant item : flowerStorage) {
            System.out.println(item);
        }
    }

    /**
     * Print all flowerpots.
     */
    private void pressMainButton3() {
        for (Plant item : flowerpotStorage) {
            System.out.println(item);
        }
    }

    /**
     * Print all flowers and flowerpots.
     * Ask customer to choose an item from list.
     * Add chosen item to the order.
     */
    private void pressOrderButton1() {
        String[] keyMenu = new String[2];
        int i = 1;
        System.out.println("Flowers:");
        for (Plant item : flowerStorage) {
            System.out.println(i + " " + item);
            i++;
        }
        System.out.println("Flowerpots:");
        for (Plant item : flowerpotStorage) {
            System.out.println(i + " " + item);
            i++;
        }
        System.out.println("Please, select menu point.");
        keyMenu[0] = input.nextLine().toUpperCase();
        if (Integer.parseInt(keyMenu[0]) < flowerStorage.size()) {
            System.out.println("Please, print amount of flowers.");
            keyMenu[1] = input.nextLine().toUpperCase();
        } else {
            keyMenu[1] = "1.0";
        }
        try {
            if (Integer.parseInt(keyMenu[0]) > flowerStorage.size()) {
                controller.addNewItem(
                        flowerpotStorage.get(Integer.parseInt(keyMenu[0]) - flowerStorage.size() - 1),
                        Double.parseDouble(keyMenu[1]));
            } else {
                controller.addNewItem(flowerStorage.get(Integer.parseInt(keyMenu[0]) - 1),
                        Double.parseDouble(keyMenu[1]));
            }
        } catch (Exception e) {
        }
    }

    /**
     * Print check of order.
     */
    private void pressOrderButton2() {
        controller.printCheck();
    }

    /**
     * Print a menu to the screen.
     *
     * @param menu is a menu(main or order).
     */
    private void outputMenu(Map<String, String> menu) {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     * Show menu and ask customer to choose a point of menu.
     */
    private void orderMenuShow() {
        String keyMenu;
        do {
            outputMenu(orderMenu);
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                orderMethodMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("B"));
    }

    /**
     * Show menu and ask customer to choose a point of menu.
     */
    public void mainMenuShow() {
        String keyMenu;
        do {
            outputMenu(mainMenu);
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                mainMethodMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
